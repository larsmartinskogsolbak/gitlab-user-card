import React from "react";
import "./Card.css";

class Card extends React.Component {
  render() {
    return (
      <div className="Card">
                <div className="image">
                    <img src={this.props.avatar_url} alt="avatar"/>
                </div>
                <div className="text">
                    {this.props.name}
                </div>
              
                <div className="text">
                    {this.props.username}
                </div>
                <div className="text">
                    {this.props.email}
                </div>
            </div>
    );
  }
}

export default Card;