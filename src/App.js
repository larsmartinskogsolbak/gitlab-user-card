import React from 'react';
import Card from "./Card"
import './App.css';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
        name: null,
        username: null,
        email: null,
        avatar_url: null,
    };
}

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Card 
            name={this.state.name} 
            username={this.state.username} 
            email={this.state.email} 
            avatar_url={this.state.avatar_url}/>
        </header>
      </div>
    );
  }

  componentDidMount() {
    if(window.location.href.includes("access_token=")) {
      let access_token = window.location.href.split("access_token=")[1].split("&")[0];

      fetch(`https://gitlab.com/api/v4/user`, {
        headers: {
          "Authorization": `Bearer ${access_token}`
        }
      })
      .then(response => {return response.json()})
      .then(data => {
          this.setState({
              name: data.name,
              username: data.username,
              email: data.email,
              avatar_url: data.avatar_url,
          })
      })
    }
    else {
      (console.log('else'))
      //window.location = `https://gitlab.com/oauth/authorize?client_id=3e4f942662973fa5e156d6e21e193d9b82a3e2f0e607d0fd673030f8c7ad2292&redirect_uri=https://gitlab-user-card.herokuapp.com/&response_type=token`;
      window.location = `https://gitlab.com/oauth/authorize?client_id=3e4f942662973fa5e156d6e21e193d9b82a3e2f0e607d0fd673030f8c7ad2292&redirect_uri=${window.location.href}&response_type=token`;
    }
  }
}

export default App;
